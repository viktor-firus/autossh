FROM ubuntu:jammy

RUN apt-get update; \
    apt-get -y dist-upgrade; \
    apt-get -y install autossh;
